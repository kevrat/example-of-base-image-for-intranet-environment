# example-of-base-image-for-intranet-environment

## Description

This is an example of how you can organize base images in a closed environment without direct internet access (perhaps only through a proxy).
There are 2 main issues solved here:

1. Custom Root Certificates. We build a shared image with custom certificates bundled.
2. Local package registries. We build a shared image for a specific OS/programming language based on the certificate image

## About

You can find more in my Telegram Channel: [@MaksimKlepikovBlog](https://t.me/MaksimKlepikovBlog)
