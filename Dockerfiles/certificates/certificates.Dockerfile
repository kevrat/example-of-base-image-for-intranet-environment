ARG BUSYBOX_IMAGE
FROM ${BUSYBOX_IMAGE} as builder
COPY certs/ /usr/local/share/ca-certificates/
RUN mkdir -p /etc/ssl/certs
RUN cat /usr/local/share/ca-certificates/* >> /etc/ssl/certs/ca-certificates.crt

FROM scratch
COPY --from=builder /usr/local/share/ca-certificates/ /usr/local/share/ca-certificates/
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt