ARG CI_REGISTRY_IMAGE
ARG CERTIFICATES_IMAGE

FROM ${CERTIFICATES_IMAGE} as certificates

# docker.io/debian:bookworm-slim
FROM ${CI_REGISTRY_IMAGE}/debian:bookworm-slim

ENV TZ="Europe/Moscow"

COPY --from=certificates /usr/local/share/ca-certificates/ /usr/local/share/ca-certificates/
COPY --from=certificates /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt

COPY local_apt_repositories.sources /etc/apt/sources.list

RUN cat /dev/null > /etc/apt/sources.list.d/debian.sources \
  && apt update && apt install -y --no-install-recommends locales ca-certificates \
  && rm -rf /var/lib/apt-get/lists/* /var/lib/apt/lists/* /tmp/* \
  && rm -rf /usr/share/man/?? \
  && rm -rf /usr/share/man/??_* \
  && echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen \
  && locale-gen

ENV LANG="en_US.UTF-8"
ENV LANGUAGE="en_US:en"