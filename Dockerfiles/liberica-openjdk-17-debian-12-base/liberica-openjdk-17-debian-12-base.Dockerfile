ARG CI_REGISTRY_IMAGE

ARG JAVA_HOME_CUSTOM=/opt/java/openjdk

# docker.io/bellsoft/liberica-openjdk-debian:17
FROM ${CI_REGISTRY_IMAGE}/bellsoft/liberica-openjdk-debian:17 as tmp_jdk
FROM tmp_jdk as jdk
ARG JAVA_HOME_CUSTOM
COPY --from=tmp_jdk $JAVA_HOME $JAVA_HOME_CUSTOM

FROM ${CI_REGISTRY_IMAGE}/debian-12-slim:stable
ARG JAVA_HOME_CUSTOM

COPY --from=jdk $JAVA_HOME_CUSTOM $JAVA_HOME_CUSTOM
ENV JAVA_HOME=$JAVA_HOME_CUSTOM
ENV PATH="${JAVA_HOME}/bin:${PATH}"

ENV JAVA_OPTIONS="-XX:+UseG1GC"

RUN keytool \ 
  -keystore $JAVA_HOME/lib/security/cacerts \
  -storepass changeit -noprompt -trustcacerts -importcert \
  -alias local_root_ca -file /usr/local/share/ca-certificates/local_root_ca.crt \
  && keytool \ 
  -keystore $JAVA_HOME/lib/security/cacerts \
  -storepass changeit -noprompt -trustcacerts -importcert \
  -alias local_intermediate_1_ca -file /usr/local/share/ca-certificates/local_intermediate_1_ca.crt
